execute pathogen#infect()

syntax on
filetype plugin indent on

set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set relativenumber
set ignorecase
set smartcase

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_javascript_eslint_exe = 'npm run lint --'

autocmd FileType make setlocal noexpandtab
autocmd FileType * set tabstop=2|set shiftwidth=2|set expandtab|set softtabstop=2
autocmd FileType python set tabstop=4|set shiftwidth=4|set expandtab
autocmd FileType cs set tabstop=4|set shiftwidth=4|set expandtab

"set <F13>=\e[[25~
set hlsearch!
nnoremap <F13> i
inoremap <F13> <Esc>
